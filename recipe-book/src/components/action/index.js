// export const incNumber = () => { type: "INCREMENT" };
// export const decNumber = () => { type: "DECREMENT" };

export const storeArray = (data) => {
  return {
    type: "STORE_DATA",
    payload: {
      data: data,
    },
  };
};

export const searchData = (data) => {
  return {
    type: "STORE_SEARCH",
    payload: data,
  };
};
