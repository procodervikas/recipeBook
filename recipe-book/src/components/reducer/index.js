import SearchData from "./searchdata";
import SaveArray from "./saveArrayData";

import { combineReducers } from "redux";

const rootReducers = combineReducers({
  SearchData,
  SaveArray,
});

export default rootReducers;
