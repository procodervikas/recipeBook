const initialState = {
  list: [],
};

const saveArray = (state = initialState, action) => {
  switch (action.type) {
    case "STORE_DATA":
      const { data } = action.payload;
      return {
        ...state,
        list: [data],
      };
    default:
      return state;
  }
};

export default saveArray;
