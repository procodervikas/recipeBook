const initialState = "";

const SearchData = (state = initialState, action) => {
  switch (action.type) {
    case "STORE_SEARCH":
      return action.payload;
    default:
      return state;
  }
};

export default SearchData;
