import React, { Component } from "react";
import { Container } from "react-bootstrap";

var newData = {
  data: "something nice",
};

var adhoc = (WrappedComponent) =>
  class extends Component {
    componentDidMount() {
      this.setState({
        data: newData.data,
      });
    }

    render() {
      return <WrappedComponent {...this.props} {...this.state} />;
    }
  };

class UnWrappedComponent extends Component {
  render() {
    return (
      <Container className="mt-5 d-flex justify-content-center">
        <h1>{this.props.data}</h1>
      </Container>
    );
  }
}

export default adhoc(UnWrappedComponent);
