import { React, useEffect, useRef } from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import "../css/main.css";
import { useDispatch } from "react-redux";
import { storeArray } from "../action/index";
import { myServices } from "../services/getData";
import { Link } from "react-router-dom";

const NavigationBar = () => {
  let textInput = useRef("");
  const dispatch = useDispatch();

  // used for read the search value and fetch the record on the basis of it
  const handleClick = async () => {
    const data = await myServices.getSearchedList(textInput.current.value);
    dispatch(storeArray(data.meals));
  };

  // fetch data from the api for first page load
  const getMyList = async () => {
    let data = await myServices.getList();
    dispatch(storeArray(data.meals));
  };

  useEffect(() => {
    getMyList();
  }, []);

  return (
    <Navbar fixed="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="#">RECIPES </Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="mr-auto my-2 my-lg-0 navbar-fixed-top"
          style={{ maxHeight: "100px" }}
        >
          <Nav.Link>
            {" "}
            <Link
              style={{ textDecoration: "none", color: "white" }}
              to="/higherOrder"
            >
              higherOrder
            </Link>
          </Nav.Link>
          <Nav.Link> Filter</Nav.Link>
          <Nav.Link>Data</Nav.Link>
        </Nav>
        <Form className="d-flex">
          <FormControl
            type="search"
            placeholder="Search"
            className="mr-2"
            aria-label="Search"
            ref={textInput}
          />
          <Button variant="outline-success" onClick={handleClick}>
            Search
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavigationBar;
