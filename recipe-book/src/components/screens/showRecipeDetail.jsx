import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ShowMoreText from 'react-show-more-text';
import '../css/main.css';

class UserCard extends React.Component {

    render() {
        const recipeObject = this.props.location.customObject;

        return (

            <Container className="margin-top">
                <Row className="cardMargin shadow-box hoverable border mt-5" >
                    <Col xs={4} className="p-2" ><img className="img" alt="description of image" src={recipeObject.image}/></Col>
                    <Col xs={8} className="p-3">
                        <div>
                            <h3 className="inline-block">{recipeObject.name}</h3>
                        </div>
                        <div className="mt-4">
                            <ShowMoreText
                                lines={3}
                                more='Show more'
                                less='Show less'
                                className='content-css show-more'
                                anchorClass='my-anchor-css-class'
                                onClick={this.executeOnClick}
                                expanded={false}

                            >
                                <p className="inline-block font-bolder" style={{ fontSize: "small" }}> {recipeObject.instructions} </p>
                            </ShowMoreText>
                        </div>
                    </Col>
                </Row>
            </Container >

        )
    }
}

export default UserCard;