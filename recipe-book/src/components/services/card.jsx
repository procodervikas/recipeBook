import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const DynamiCard = (props) => {
  return (
    <Card style={{ width: "15rem" }} className="p-2">
      <Card.Img variant="top" src={props.recipeObject.strMealThumb} />
      <Card.Body>
        <Card.Title>{props.recipeObject.strMeal}</Card.Title>
        <Button variant="primary">
          <Link
            className="anchor"
            to={{
              pathname: "/usercard",
              customObject: {  
                image: props.recipeObject.strMealThumb,
                name: props.recipeObject.strMeal,
                instructions: props.recipeObject.strInstructions,
              },
            }}
          >
            View
          </Link>
        </Button>
      </Card.Body>
    </Card>
  );
};

export default DynamiCard;
