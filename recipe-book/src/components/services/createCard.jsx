import { React, useState } from "react";
import Cards from "./card";
import { Container, Row, Col, CardColumns } from "react-bootstrap";
import { useSelector } from "react-redux";
import Pagination from "react-bootstrap/Pagination";
import Spinner from "./loader";

const CreateCard = () => {
  let recipeData = useSelector((state) => state.SaveArray.list[0]) || [];
  // const [sortRecipe, setSortRecipe] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const dataPerPage = 12;
  const pageVisited = pageNumber * dataPerPage;
  let pageLength;

  // setSortRecipe(recipeData);
  // console.log(sortRecipe);
  // it slices the data as per as the page number
  const displayData = recipeData
    // .filter(meals => meals.strMeal.includes('Chocolate'))
    .slice(pageVisited, pageVisited + dataPerPage)
    .map((element, index) => {
      return (
        <Cards className="d-flex flex-row" recipeObject={element} key={index} />
      );
    });

  // const sortData = (action) => {
  //   console.log(action.target.innerText);
  //   if (action.target.innerText === "A-Z") {
  //     setSortRecipe(
  //       recipeData.sort((a, b) => (a.strMeal > b.strMeal ? 1 : -1))
  //     );
  //   } else {
  //     setSortRecipe(
  //       recipeData.sort((a, b) => (a.strMeal > b.strMeal ? -1 : 1))
  //     );
  //   }
  // };

  //it change the next page number one by one
  const changePage = (event) => {
    setPageNumber(event.target.text - 1);
  };

  // it changes the previous page one by one
  const previousPage = () => {
    setPageNumber(pageNumber - 1);
  };

  // it changes the forward page one by one
  const forwardPage = () => {
    setPageNumber(pageNumber + 1);
  };

  // it set first page when click
  const pageFirst = () => {
    setPageNumber(0);
  };

  const pageLast = () => {
    setPageNumber(pageLength - 1);
  };

  // it sets the counter for the page for pagination number
  const PageCounter = () => {
    const pageCounter = Math.ceil(recipeData.length) / dataPerPage;

    let items = [];
    pageLength = Math.round(pageCounter);
    for (let number = 1; number <= pageLength; number++) {
      items.push(
        <Pagination.Item key={number} onClick={(event) => changePage(event)}>
          {number}
        </Pagination.Item>
      );
    }
    return items;
  };
  const items = PageCounter();

  return (
    <>
      <Spinner
        status={recipeData.length === 0 ? "true" : "false"}
        style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
      />

      <Container style={{ marginTop: "5rem" }}>
        <Row className="justify-content-center">
          <Col className="mt-3">
            {recipeData.length === 0 && (
              <h1 className="text-center">OOPS! Sorry Recipe Not Found</h1>
            )}
            <CardColumns className="d-flex flex-row flex-wrap justify-content-center">
              {displayData}
            </CardColumns>
          </Col>
        </Row>
      </Container>
      <Pagination className="d-flex justify-content-center">
        <Pagination.First disabled={pageNumber === 0} onClick={pageFirst} />
        <Pagination.Prev disabled={pageNumber === 0} onClick={previousPage} />
        {items}
        <Pagination.Next
          disabled={pageNumber === pageLength - 1}
          onClick={forwardPage}
        />
        <Pagination.Last
          disabled={pageNumber === pageLength - 1}
          onClick={pageLast}
        />
      </Pagination>
    </>
  );
};

export default CreateCard;
