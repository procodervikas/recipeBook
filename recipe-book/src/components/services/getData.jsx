export const myServices = {
  getList,
  getSearchedList,
};

function getList() {
  return fetch("https://www.themealdb.com/api/json/v1/1/search.php?f=c").then(
    (data) => data.json()
  );
}

function getSearchedList(typedValue) {
  return fetch(
    "https://www.themealdb.com/api/json/v1/1/search.php?s=" + typedValue
  ).then((data) => data.json());
}
