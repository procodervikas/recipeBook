import { Container } from "react-bootstrap";
import Loader from "react-loader-spinner";

const Spinner = (props) => {

  return (
    <Container
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)",
      }}
    >
      <Loader
        type="Puff"
        color="#00BFFF"
        height={100}
        width={100}
        timeout={3000}
        visible={props.status}
      />
    </Container>
  );
};

export default Spinner;
