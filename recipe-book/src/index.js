import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import NavigationBar from '../src/components/screens/nav';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Recipes from "../src/components/screens/recipeCard";
import UserCard from '../src/components/screens/showRecipeDetail';
import HigherOrderCom from '../src/components/screens/hoc'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from './components/store';
import { Provider } from 'react-redux';

var element = (
    <Router>
        <Provider store={store}>
            <NavigationBar />
            <Switch>
                <Route exact path="/userCard" component={UserCard} />
                <Route exact path="/" component={Recipes} />
                <Route exact path="/higherOrder" component={HigherOrderCom} />
            </Switch>
        </Provider>
    </Router>
)

ReactDOM.render(element, document.getElementById('root'));